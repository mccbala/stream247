var http = require('request'),
    ch = require('cheerio'),
    path = require('path'),
    fs = require('fs'),
    async = require('async');
    
var dt = new Date();

var DEBUG = false,
    FOLDER = '/tmp/stream247-'+[dt.getFullYear(), dt.getMonth()+1, dt.getDate(), ''].join('-'),
    website = 'http://onwatchseries.to',
    options = {
        site: null,
        series: null,
        season: null,
        episode: null,
        url: null,
        seriesData: null,
        seriesPageData: null,
        requestType: null,
    },
    acceptable_sites = [
        'gorillavid.in',
        'daclips.in',
        'movpod.in',
        'playedto.me'
    ],
    i = 0;

if (process.argv.length > 2) {
    while (i < process.argv.length) {

        var k = process.argv[i];
        var v = process.argv[i + 1];

        switch (k.toLowerCase()) {
            case '--site':
            case '--series':
                options[k.replace('--', '')] = v;
                break;
            case '--season':
            case '--episode':
                options[k.replace('--', '')] = parseInt(v) - 1;
                break;
            case '--debug':
                DEBUG = true;
                break;
            case '--print-file':
                options.printFile = true;
                break;
            default:
                _l('Unrecognized switch ' + k);
                break;
        }
        i++;
    }
    getVideo(options, (err, data) => {
        if (err)
            console.error(err);
        else
            console.log(JSON.stringify(data.selectedEpisode));
    });
}

module.exports = {
    getVideo: getVideo
};

function getVideo(options, callback) {
    console.time("totalTime");
    async.waterfall([
        (callback) => {
            callback(null, options);
        },
        setUrl,
        getDataFromFile,
        downloadSeriesPage,
        compileSeries,
        episodeExists,
        downloadEpisodePage,
        compileEpisodeLinks,
        getVideoData,
        updateDataInFile
    ], (err, data) => {
        console.timeEnd("totalTime");
        callback(err, data);
    });
}

function setUrl(options, callback) {
    _l(options);
    if (!website && !options.season && !options.series)
        _e('Invalid request!');
    else /* if (!options.season) { */
        options.url = [website, 'serie', options.series].join('/');
    /*options.requestType = 'series';
    } else if (!options.episode) {
        options.url = ['http:/', website, options.series, 'season-' + options.season].join('/');
        options.requestType = 'season';
    } else {
        options.url = ['http:/', website, 'episode', options.series + '_s' + options.season + '_e' + options.episode + '.html'].join('/');
        options.requestType = 'episode';
    }*/
    callback(null, options);
}

function getVideoData(options, callback) {
    _l(options);
    var links = options.selectedEpisode.videoLinks;
    if (links.length < 1) {
        callback('No video links!', options);
        return;
    }
    var link = links.shift();
    console.time("Get Video Data");
    getPage(link, (err, data) => {
        if (err) {
            if (options.selectedEpisode.videoLinks.length > 1) {
                _e(link + ' not working! Skipping to next link');
                getVideoData(options, callback);
            } else
                callback('No links available!', options);
        }
        var $ = ch.load(data),
            formdata = {};
        $('input[type=hidden]').each((i, input) => {
            formdata[$(input).attr('name')] = $(input).attr('value');
        });
        getPage(link, (err, data) => {
            var $ = ch.load(data);
            if (data.indexOf('flvplayer') > 0) {
                var videoUrl = /http:\/\/.*?\.(mp4|flv)/im.exec(data).shift();
                var imageUrl = /http:\/\/img.*?\.(jpg)/im.exec(data).shift().split('/').pop().replace('.jpg', '');
                imageUrl = new Buffer(imageUrl, 'base64').toString() + '.jpg';
                http.head(videoUrl, (error, response, body) => {
                    if (response.statusCode == 200) {
                        options.selectedEpisode.videoPageUrl = link;
                        options.selectedEpisode.videoUrl = videoUrl;
                        options.selectedEpisode.imageUrl = imageUrl;
                        delete options.selectedEpisode.videoLinks;
                        callback(null, options);
                    } else {
                        var s = options.season;
                        var e = options.episode;
                        var p = options.seriesData.seasons[s].episodes[e].videoLinks.indexOf(link);
                        //options.seriesData.seasons[s].episodes[e].videoLinks.splice(p, 1);
                        getVideoData(options, callback);
                        console.log("Getting video link failed.")
                        console.timeEnd("Get Video Data");
                    }
                });
            }
        }, 'POST', formdata);
    });
}

function compileSeries(options, callback) {
    _l(options);
    if (!options.seriesData) {
        var $ = ch.load(options.seriesPageData);
        var s = options.series;
        var seriesData = {
            title: s.replace(/_/ig, ' ').replace(/\w\S*/ig, (t) => {
                return t.charAt(0).toUpperCase() + t.substr(1).toLowerCase();
            }),
            seasonsCount: $('div[itemprop=season]').length,
            seasons: []
        };
        $('div[itemprop=season]').each((i, sdata) => {
            seriesData.seasons[i] = {
                seasonNumber: parseInt($(sdata)
                    .find('h2.lists span[itemprop=name]')
                    .text()
                    .replace('Season ', '')),
                seasonLink: $(sdata)
                    .find('h2.lists a[itemprop=url]')
                    .prop('href'),
                episodes: []
            };
            $(sdata).find('ul li').each((j, edata) => {
                seriesData.seasons[i].episodes[j] = {
                    episodeTitle: $(edata)
                        .find('span[itemprop=name]')
                        .text()
                        .replace(/^\s+|\s+$|^Episode\s\d+\s+/ig, ''),
                    episodeNumber: parseInt($(edata)
                        .find('meta[itemprop=episodenumber]')
                        .prop('content')),
                    episodeLink: website + $(edata)
                        .find('meta[itemprop=url]')
                        .prop('content').replace(website, ''),
                    videoLinks: []
                };
            });
            seriesData.seasons[i].episodes.reverse();
            seriesData.seasons[i].episodeCount = seriesData.seasons[i].episodes.length;
        });
        seriesData.seasons.reverse();
        options.seriesData = seriesData;
        options.dataUpdated = true;
        delete options.seriesPageData;
    }
    callback(null, options);
}

function compileEpisodeLinks(options, callback) {
    _l(options);
    var videoLinksExist = options.selectedEpisode
        .videoLinks
        .length > 0
    var videoLinks = [];
    if (!videoLinksExist) {
        var $ = ch.load(options.episodePageData);
        $('a.buttonlink').each((l, link) => {
            var title = $(link).attr('title');
            if (acceptable_sites.indexOf(title) >= 0) {
                var link = new Buffer($(link).attr('href').split('=').pop(), 'base64').toString();
                if (link.trim().length > 0)
                    videoLinks.push(link);
            }
        });
        options
            .seriesData
            .seasons[options.season]
            .episodes[options.episode]
            .videoLinks = videoLinks;
        options
            .selectedEpisode
            .videoLinks = videoLinks;
        options.dataUpdated = true;
        options.videoLinksUpdated = true;
        delete options.episodePageData;
        console.log(options.selectedEpisode);
    }
    callback(null, options);
}

function downloadSeriesPage(options, callback) {
    _l(options);
    if (!options.seriesData) {
        console.time("Download Series Page");
        getPage(options.url, (err, data) => {
            console.timeEnd("Download Series Page");
            options.seriesPageData = data;
            callback(err, options);
        });
    } else
        callback(null, options);
}

function downloadEpisodePage(options, callback) {
    _l(options);
    if (options.selectedEpisode.videoLinks.length > 0)
        callback(null, options);
    else {
        console.time("Download Episode Page");
        getPage(options.selectedEpisode.episodeLink, (err, data) => {
            console.timeEnd("Download Episode Page");
            options.episodePageData = data;
            callback(null, options);
        });
    }
}

function getDataFromFile(options, callback) {
    _l(options);
    console.time("Read File");
    fs.readFile(FOLDER + options.series, 'utf8', (err, data) => {
        options.seriesData = err ? false : JSON.parse(data);
        console.log(err ? 'File not found' : 'File found');
        console.timeEnd("Read File");
        callback(null, options);
    });
}

function updateDataInFile(options, callback) {
    _l(options);
    if (options.printFile)
        _i(options.seriesData);
    if (options.dataUpdated) {
        console.time("Update File");
        fs.writeFile(FOLDER + options.series, JSON.stringify(options.seriesData), () => {
            console.timeEnd("Update File");
            callback(null, options);
        });
    } else
        callback(null, options);
}

function episodeExists(options, callback) {
    _l(options);
    options.season = parseInt(options.season) > 0 ? options.season - 1 : Math.floor(Math.random() * (options.seriesData.seasonsCount - 1));
    var s = options.season;
    options.episode = parseInt(options.episode) > 0 ? options.episode - 1 : Math.floor(Math.random() * (options.seriesData.seasons[s].episodeCount - 1));
    var e = options.episode;
    var exists = between(s, 0, options.seriesData.seasonsCount - 1) && between(e, 0, options.seriesData.seasons[s].episodeCount - 1);
    if (exists) {
        options.selectedEpisode = JSON.parse(JSON.stringify(options.seriesData.seasons[s].episodes[e]));
        options.selectedEpisode.title = options.seriesData.title;
        options.selectedEpisode.previousEpisodeExists = between(e - 1, 0, options.seriesData.seasons[s].episodeCount - 1);
        options.selectedEpisode.nextEpisodeExists = between(e + 1, 0, options.seriesData.seasons[s].episodeCount - 1);
        s += 1;
        e += 1;
        options.selectedEpisode.fullTitle = [(parseInt(s) < 10 ? '0' : '') + (s) + 'x' + (parseInt(e) < 10 ? '0' : '') + e, options.selectedEpisode.episodeTitle, options.seriesData.title].join(' - ');
        options.selectedEpisode.seasonNumber = parseInt(s);
        callback(null, options);
    } else
        callback('Episode does not exist!', options);
}

function getPage(url, callback, method, formdata) {
    var req = http({
        url: url,
        method: method ? method : 'GET',
        form: formdata,
        jar: true,
        headers: {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36'
        }
    }, (err, res, body) => {
        _l('HTTP Response');
        if (err)
            _e(err);
        callback(err, body);
    });
    req.on('err', (e) => {
        _l(e);
        setTimeout(() => {
            getPage(url, callback, method);
        }, 6000);
    });
    _l('HTTP Request - ' + url);
}

function _l(msg) {
    if (DEBUG)
        console.log(msg);
}

function _i(msg) {
    console.info(msg);
}

function _e(msg) {
    console.error(msg);
}

function between(x, min, max) {
    return x >= min && x <= max;
}
