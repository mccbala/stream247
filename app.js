const http = require('http'),
    fs = require('fs'),
    path = require('path'),
    contentTypes = require('./utils/content-types'),
    sysInfo = require('./utils/sys-info'),
    watchseries = require('./watchseries'),
    querystring = require('querystring'),
    env = process.env; 

var server = http.createServer(function(req, res) {
    var url = req.url;
    if (url == '/') {
        url += 'player.html';
    }

    // IMPORTANT: Your application HAS to respond to GET /health with status 200
    //            for OpenShift health monitoring

    if (url == '/health') {
        res.writeHead(200);
        res.end();
    } else if (url == '/info/gen' || url == '/info/poll') {
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('Cache-Control', 'no-cache, no-store');
        res.end(JSON.stringify(sysInfo[url.slice(6)]()));
    } else if (/\/series\/?.*\/?/.test(url)) {
        var requestdata = querystring.parse(url.split('?').pop());
        var format = requestdata.format ? requestdata.format : 'json';
        var acceptEncoding = req.headers['content-encoding'];
        if (requestdata.series) {
            watchseries.getVideo(requestdata, (error, data) => {
                var responsedata = '';
                switch (format) {
                    default:
                        case 'json':
                        res.setHeader("Content-Type", "application/json");
                    responsedata = JSON.stringify(data.selectedEpisode);
                    break;
                    case 'text':
                            responsedata = data.selectedEpisode.videoUrl;
                        break;
                    case 'download':
                            res.writeHead(302, {
                            'Location': data.selectedEpisode.videoUrl
                        });
                        break;
                }
                res.end(responsedata);
            });
        }
    } else {
        fs.readFile('./static' + url, function(err, data) {
            if (err) {
                res.writeHead(404)
                res.end('Not found');
            } else {
                var ext = path.extname(url).slice(1);
                res.setHeader('Content-Type', contentTypes[ext]);
                if (ext === 'html') {
                    res.setHeader('Cache-Control', 'no-cache, no-store');
                }
                res.end(data);
            }
        });
    }
});

server.listen(env.NODE_PORT || 3000, env.NODE_IP || 'localhost', function() {
    console.log(`Application worker ${process.pid} started...`);
});